//window.location.search returns the query string part of the URL
// console.log(window.location.search)

//instantiate a URLSearchParams object so we can execute methods to access parts of the query string
let params = new URLSearchParams(window.location.search)

// console.log(params.get('courseId'))

let courseId = params.get('courseId')

//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");

//retrieve the userId stored in local Storage
let userId = localStorage.getItem("id");

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");

fetch(`http://localhost:3000/api/courses/${courseId}`)
.then((res) => res.json())
.then((data) => {
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	enrollContainer.innerHTML = '<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>'

	let enrollButton =
	document.querySelector("#enrollButton")

	enrollButton.addEventListener("click", () => {
		//insert the course to our course
		fetch('http://localhost:3000/api/users/enroll', {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId,
				userId: userId
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true){
				//enrollement is successful
				alert("Thank you for enrolling! See you!")
				window.location.replace('./course.html')
			}else{
				alert("Enrollment failed")
			}
		})
	})
})




